#!/usr/bin/env python3

from s2_py import serial_open, serial_close, s2_serial_setup, s2_check_API_compatible, S2_BAUD, NULL
import logging
from logging import info, error
logging.getLogger().setLevel(logging.INFO)
from s2_local_settings import s2_port_name


s2port = serial_open(s2_port_name.encode('utf-8'))
if s2port==NULL:
    error('can not connect to S2')
    exit(1)
s2_serial_setup(s2port, S2_BAUD)

info('compatibility test: {}'.format(s2_check_API_compatible(s2port)))

serial_close(s2port)
