#!/usr/bin/env python3

from s2_py import serial_open, serial_close, s2_serial_setup, s2_query_info, s2_set_settings, s2_query_settings, \
    S2_info, S2_settings, S2_PULSING_INTERNAL, S2_BAUD, NULL
import logging
from logging import info, error
logging.getLogger().setLevel(logging.INFO)
from s2_local_settings import s2_port_name

s2port = serial_open(s2_port_name.encode('utf-8'))
if s2port == NULL:
    error('can not connect to S2')
    exit(1)
s2_serial_setup(s2port, S2_BAUD)

s2s = S2_settings()
s2_query_settings(s2port, s2s)
s2s.output_voltage_set = 2.5
s2s.pulsing_mode = S2_PULSING_INTERNAL
s2_set_settings(s2port, s2s, False)

s2i = S2_info()

for i in range(10):
    s2_set_settings(s2port, s2s, False)

    s2_query_info(s2port, s2i)
    info((s2i.output_voltage_measured, s2i.output_current_measured))

serial_close(s2port)
