from s2_cffi.lib import *
from s2_cffi import ffi

NULL = ffi.NULL


def S2_settings():
    s2s = ffi.new('S2_settings *')
    init_s2_settings(s2s)
    return s2s


def S2_info():
    return ffi.new('S2_info *')


def print_s2_settings(s2s):
    for f in dir(s2s):
        print(f,)
        eval('print(s2s.{})'.format(f))


if __name__ == '__main__':
    s2s = S2_settings()
    print_s2_settings(s2s)