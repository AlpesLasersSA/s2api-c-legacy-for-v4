import platform
import os
from cffi import FFI

ffibuilder = FFI()

rel_path = 'c_interface'
cdef = lambda file_name, packed=False: \
    ffibuilder.cdef(open(os.path.join(os.path.abspath(os.path.curdir),
                                      rel_path, file_name)).read(), packed=packed)

ffibuilder.cdef("#define S2_BAUD ...") # from s2_interface.h
cdef('_s2_defs.h', True)
cdef('_serial.h')
if platform.system() == "Windows":
    ffibuilder.cdef('typedef struct _DCB {...;} DCB;')
    cdef('_struct_serial_w32.h')
else:
    ffibuilder.cdef('typedef struct termios {...;};')
    cdef('_struct_serial_posix.h')
cdef('_s2_interface.h')
cdef('_checksum.h')
cdef('_slip.h')

source_str = ''
for fn in ['checksum', 's2_interface', 'serial_platform', 'slip']:
    source_str += '#include "{}"\n'.format(os.path.join(os.path.abspath(os.path.curdir), rel_path, fn+'.c'))
ffibuilder.set_source("s2_cffi", source_str)

if __name__ == "__main__":
    ffibuilder.compile(verbose=True)
