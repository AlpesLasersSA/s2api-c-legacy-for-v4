/** @file **/

#include <math.h>

float   max_current(int hardware_version, float period, float width, float max_peak_current)
{
    if(period==0)
        return 0;

    float duty = width/period;
    if(duty>1)
        duty=1;

    if(hardware_version==4)
        return fmin(max_peak_current, 3 + 20*pow(2, -8*duty));

    return 0;
}
