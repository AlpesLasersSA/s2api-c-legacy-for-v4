#include <stdio.h>
#include "s2_interface.h"

void print_s2_settings(S2_settings *s)
{
    printf("pp\t\t%d\n",  s->pulse_period);
    printf("pw\t\t%d\n",  s->pulse_width);
    printf("sw\t\t%d\n",  s->sync_out_width);
    printf("ov\t\t%f\n",  s->output_voltage_set);
    printf("ocl\t\t%f\n", s->output_current_limit);
    printf("pm\t\t%d\n",  s->pulsing_mode);
    printf("bt\t\t%f\n",  s->bias_t);
    printf("ppp\t\t%d\n", s->pulse_period_prescaler);
    printf("pwp\t\t%d\n", s->pulse_width_prescaler);
    printf("bon\t\t%d\n", s->burst_ON);
    printf("boff\t%d\n",  s->burst_OFF);
}

int main()
{
    S2_settings s;
    init_s2_settings(&s);
    print_s2_settings(&s);
}