struct serial {
	int			fd;
	struct termios		oldtio;
	struct termios		newtio;

	char			configured;
	serial_baud_t		baud;
	serial_bits_t		bits;
	serial_parity_t		parity;
	serial_stopbit_t	stopbit;
};