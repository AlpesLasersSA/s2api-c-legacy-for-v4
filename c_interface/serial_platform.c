/** @file **/

#if defined(_WIN32) || defined(__CYGWIN__)
#   include "serial_w32.h"
#else
#   include "serial_posix.h"
#endif
