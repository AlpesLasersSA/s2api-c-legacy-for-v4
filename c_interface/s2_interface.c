/** @file **/


#include "s2_interface.h"
#include "checksum.h"
#include "slip.h"


/**
 * @brief sets correct serial port parameters: 8-N-1
 * @param port valid open serial port handle
 * @param baud baud rate to set (default = #S2_BAUD)
 * @return 0 on success, otherwise !=0
 */
serial_err_t s2_serial_setup(serial_t *port, serial_baud_t baud)
{
    return serial_setup(port, baud, SERIAL_BITS_8, SERIAL_PARITY_NONE, SERIAL_STOPBIT_1);
}


/**
 * @brief internal lower level function, handles the data exchange with s2
 * @param port #serial_t * serial port handle
 * @param tx_type data type to transmit (value of #S2_packet_type)
 * @param tx_data data to transmit
 * @param tx_size tx_data size in bytes (max. #S2_PACKET_SIZE-4)
 * @param rx_type data type to receive (value of #S2_packet_type)
 * @param rx_data pointer to memory where received data will be stored (will be discarded if NULL)
 * @param rx_size rx_data size in bytes (max. #S2_PACKET_SIZE-4)
 * @param rx_timeout timeout in microseconds to wait for reply
 * @return 0 on success, value from errno.h on errors
 */
int s2_exchange(serial_t *port,
                uint16_t tx_type, void *tx_data, uint8_t tx_size,
                uint16_t rx_type, void *rx_data, uint8_t rx_size, unsigned int rx_timeout)
{
    if(tx_size>S2_PACKET_SIZE-2*sizeof(uint16_t)||rx_size>S2_PACKET_SIZE-2*sizeof(uint16_t))
        return EMSGSIZE;

    uint8_t tx_buffer[S2_PACKET_SIZE];
    memcpy(tx_buffer, &tx_type, 2);
    if(tx_size>0)
        memcpy(tx_buffer+2, tx_data, tx_size);
    uint16_t ch = fletcher16(tx_buffer, S2_PACKET_SIZE-2);
    memcpy(tx_buffer+S2_PACKET_SIZE-2, &ch, 2);
    if(SERIAL_ERR_OK!=SLIP_send_buffer(port, tx_buffer, S2_PACKET_SIZE))
        return EIO;

    if(rx_size>0)
    {
        uint8_t rx_buffer[S2_PACKET_SIZE];
        if(SERIAL_ERR_OK!=SLIP_receive_buffer(port, rx_buffer, S2_PACKET_SIZE, rx_timeout))
            return EIO;
        uint16_t t;
        memcpy(&t, rx_buffer+S2_PACKET_SIZE-2, sizeof(t));
        if(t != fletcher16(rx_buffer, S2_PACKET_SIZE-2))
            return EFAULT;
        memcpy(&t, rx_buffer, sizeof(t));
        if(t != rx_type)
            return EILSEQ;
        if(rx_data!=NULL)
            memcpy(rx_data, rx_buffer+2, rx_size);
    }

    return SERIAL_ERR_OK;
}

/**
 * @brief initializes settings structure to default safe values
 * @param s structure to initialize
 */
void init_s2_settings(S2_settings *s)
{
    memset(s, 0, sizeof(*s));
    s->pulse_period_prescaler = 1;
    s->pulse_width_prescaler = 1;
    s->output_voltage_set = 1.0;
    s->output_current_limit = 0.1;
    s->pulse_period = 500;
    s->pulse_width = 1;
    s->pulsing_mode = S2_PULSING_OFF;
}


/**
 * @brief query actual settings from s2
 * @param port serial port handle
 * @param s pointer to settings structure which will receive the data
 * @return
 */
int s2_query_settings(serial_t *port, S2_settings *s)
{
    return s2_exchange(port, S2_PACKET_QUERY_SETTINGS, NULL, 0,
                       S2_PACKET_QUERY_SETTINGS, (void*)s, sizeof(*s), TIMEOUT_RX);
}


/**
 * @brief set s2 settings
 * @param port serial port handle
 * @param s pointer to settings structure holding new settings
 * @param persist - if true, settings are stored in non-volatile memory and then applied at board startup; then function call takes ~2s
 * @return
 */
int s2_set_settings(serial_t* port, S2_settings *s, bool persist)
{
    return s2_exchange(port,
                       persist?S2_PACKET_SET_PERSISTENT_SETTINGS:S2_PACKET_SET_SETTINGS, s, sizeof(*s),
                       S2_PACKET_QUERY_SETTINGS, NULL, sizeof(*s), persist?TIMEOUT_RX_FLASH:TIMEOUT_RX);
}

/**
 * @brief resets flags like overcurrent
 * @param port serial port handle
 * @param flag to reset
 * @return
 */
int s2_reset_status_flag(serial_t* port, uint16_t flag)
{
    return s2_exchange(port, S2_PACKET_RESET_STATUS_FLAG, &flag, sizeof(flag),
                       S2_PACKET_RESET_STATUS_FLAG, NULL, sizeof(S2_info), TIMEOUT_RX);
}


/**
 * @brief queries measured values and additional information from S2
 * @param port serial port handle
 * @param i structure which will accept the information
 * @return
 */
int s2_query_info(serial_t* port, S2_info *i)
{
    return s2_exchange(port, S2_PACKET_INFO, NULL, 0,
        S2_PACKET_INFO, (void*)i, sizeof(*i), TIMEOUT_RX);
}


/**
 * @brief checks if this version of S2 API is fully compatible with the boards API
 * @param port
 * @return 0 if compatible; 1 if board is too new; -1 if board is too old
 */
int s2_check_API_compatible(serial_t *port)
{
    S2_info s2i;
    s2_query_info(port, &s2i);
    if(s2i.API_version > S2_API_VERSION)
        return 1;
    else if(s2i.API_version < S2_API_VERSION)
        return -1;

    return 0;
}

