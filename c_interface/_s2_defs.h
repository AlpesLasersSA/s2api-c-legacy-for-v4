/**
 * @brief Contains possible pulsing modes; used in S2_settings::pulsing_mode
 */
enum S2_pulsing_mode {
    S2_PULSING_OFF = 0,
    S2_PULSING_INTERNAL = 1,
    S2_PULSING_EXTERNAL = 2,
    S2_PULSING_BURST = 3,
    // 4 is reserved for custom boards
    // 5 is reserved for custom boards
    S2_PULSING_BURST_EXTERNAL = 6
};


enum S2_packet_type {
    S2_PACKET_INFO = 0,
    S2_PACKET_QUERY_SETTINGS = 1,
    S2_PACKET_SET_SETTINGS = 2,
    S2_PACKET_SET_PERSISTENT_SETTINGS = 4,
    S2_PACKET_RESET_STATUS_FLAG = 5
};


enum S2_status {
    S2_STATUS_OK = 0,
    S2_STATUS_UNDERVOLTAGE = 1,
    S2_STATUS_OVERCURRENT = 2
};


// float is assumed 32-bit according to IEEE 754

typedef struct {
    uint16_t pulse_period; ///< pulse period length in 20 ns increments
    uint16_t pulse_width; ///< output pulse width in 20 ns increments; width>=period enables CW output
    uint16_t sync_out_width; ///< sync pulse width in 20 ns increments
    float output_voltage_set; ///< output voltage in volts, ~1 to 25 V
    float output_current_limit; ///< output current limit in amps
    uint16_t pulsing_mode; ///< output mode: takes values from #S2_pulsing_mode enum
    uint8_t reserved_1; ///< reserved, set to 0
    float bias_t; ///< TODO
    uint8_t reserved_2[3]; ///< reserved, set to 0
    uint16_t pulse_period_prescaler; ///< TODO
    uint16_t pulse_width_prescaler; ///< TODO
    uint32_t burst_ON; ///< TODO
    uint32_t burst_OFF; ///< TODO
} S2_settings;


typedef struct {
    uint32_t device_id; ///< unique ID of the board
    uint16_t sw_version; ///< board firmware version
    uint16_t hw_version; ///< board hardware version
    float input_voltage_measured; ///< measured input (power supply) voltage
    float output_voltage_measured; ///< measured output (pulse amplitude) voltage
    float output_current_measured; ///< measured pulse current
    float MCU_temperature; ///< board controller temperature
    float laser_temperature; ///< laser temperature (if connected)
    uint8_t reserved_1[16]; ///< reserved, do not use
    float output_current_measured_out_of_pulse; ///< TODO
    uint16_t status; ///< indicates possible faults, such as overcurrent
    uint32_t pulse_clock_frequency; ///< a constant indicating pulse timer resolution (50MHz -> 20ns)
    uint32_t API_version; ///< board API version; has #S2_API_VERSION value if board has same API version
} S2_info;
