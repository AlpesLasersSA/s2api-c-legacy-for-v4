/** @file **/


#ifndef C_INTERFACE_S2_DEFS_H
#define C_INTERFACE_S2_DEFS_H

#include "s2_api_version.h"

#define S2_PACKET_SIZE 64

// TODO: has to be fetched from s2
#define S2_CLOCK 50e6
#define S2_TS (1e9/S2_CLOCK)

#define S2_MAX_PEAK_CURRENT_GEN4 8.0

#pragma pack(push,1)

#include "_s2_defs.h"

#pragma pack(pop)

#endif //C_INTERFACE_S2_DEFS_H
