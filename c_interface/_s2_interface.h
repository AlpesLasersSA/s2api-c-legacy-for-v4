serial_err_t s2_serial_setup(serial_t *port, serial_baud_t baud);

int s2_exchange(serial_t *port,
                uint16_t tx_type, void *tx_data, uint8_t tx_size,
                uint16_t rx_type, void *rx_data, uint8_t rx_size, unsigned int rx_timeout);

void init_s2_settings(S2_settings *s);
int s2_query_settings(serial_t *port, S2_settings *s);
int s2_set_settings(serial_t *port, S2_settings *s, bool persist);
int s2_reset_status_flag(serial_t *port, uint16_t flag);
int s2_query_info(serial_t *port, S2_info *i);
int s2_check_API_compatible(serial_t *port);
