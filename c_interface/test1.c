#include <stdio.h>
#include <unistd.h>
#include "s2_interface.h"

#if defined(__WIN32__) || defined(__CYGWIN__)
#define SERIAL_PORT "COM6"
#else
#define SERIAL_PORT "/dev/ttyUSB0"
#endif



int main()
{
    S2_settings s2_settings;
    S2_info s2_info;
    serial_t *s;
    int ret;

    s = serial_open(SERIAL_PORT);
    if(s==NULL)
    {
        printf("warning: can not open test port: %s\n", SERIAL_PORT);
        return 1;
    }
    s2_serial_setup(s, S2_BAUD);

    printf("0: compatibility test: %d\n", s2_check_API_compatible(s));
    s2_query_info(s, &s2_info);
    printf("%d %d\n", S2_API_VERSION, s2_info.API_version);


    puts("1: querying current S2 settings...");
    ret = s2_query_settings(s, &s2_settings);
    if(!ret) {
        //for example: read voltage
        printf("Current output voltage setting: %f\n", s2_settings.output_voltage_set);
        printf("Current prescalers: %d, %d\n", s2_settings.pulse_period_prescaler, s2_settings.pulse_width_prescaler);
        puts("done.\n");
    }
    else
        printf("error %d\n", ret);


    puts("2: sending S2 settings...");
    init_s2_settings(&s2_settings);
    ret = s2_set_settings(s, &s2_settings, true);
    if(!ret)
        puts("done.\n");
    else
        printf("error %d\n", ret);

    puts("3: querying S2 info...");
    ret = s2_query_info(s, &s2_info);
    if(!ret) {
        //for example: read device ID
        printf("Device ID: %d\n", s2_info.device_id);
        printf("HW/SW: %d/%d\n", s2_info.hw_version, s2_info.sw_version);
        printf("Output voltage: %fV\n", s2_info.output_voltage_measured);
        printf("Output current: %fA\n", s2_info.output_current_measured);
        printf("Status: %d\n", s2_info.status);
        puts("done.\n");
    }
    else
        printf("error %d\n", ret);



    usleep(100000);
    serial_close(s);


    return 0;
}

