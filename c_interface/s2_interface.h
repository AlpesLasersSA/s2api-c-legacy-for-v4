/** @file **/

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include "serial.h"
#include "s2_defs.h"

#define S2_BAUD SERIAL_BAUD_38400
#define TIMEOUT_RX 100000 //< 0.1 second for normal operations
#define TIMEOUT_RX_FLASH 5000000 //< 5 seconds if MCU has to write into flash (i.e. store settings)

#include "_s2_interface.h"
