serial_err_t SLIP_send_buffer(serial_t* port, uint8_t *buffer, unsigned int length);
serial_err_t SLIP_receive_buffer(serial_t* port, uint8_t *buffer,
                                 unsigned int length, unsigned int timeout_us);
