#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on August 27, 2015

Copyright Alpes Lasers SA, Neuchatel, Switzerland, 2015

@author: chiesa
"""

from setuptools import setup, find_packages

setup(
        name="s2_py",
        version="3.1.9",
        packages=find_packages(),
        setup_requires=["cffi>=1.0.0"],
        test_suite="s2_py.tests",
        cffi_modules=["s2_py/build.py:ffibuilder"],
        install_requires=["cffi>=1.0.0"],
)
